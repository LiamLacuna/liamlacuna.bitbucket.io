'use strict'

/**
 * This function creates a object Node that contains two Child Node
 * @param {*} xIn x coordinate of new object Node
 * @param {*} yIn y coordinate of new object Node
 */
function createObjectNode (xIn, yIn) {
    let x = xIn
    let y = yIn
    let lines = []
    const width = 150
    const height = 180
    let child1 = createChildNode(x + 30, y + 25, 'Object name')
    let child2 = createChildNode(x + 30, y + 55, 'Attribute name')
    return {
	getChild1: () => {
	    return child1
	},
	getChild2: () => {
	    return child2
	},
	getBounds: () => {
	    return {
		x: x,
		y: y,
		width: 150,
		height: 180
	    }
	},
	addlines:(l)=>{
	    lines.push(l)
	},
	getlines:()=>{
	    return lines
	},
	contains: p => {
	    if (p.x >= x && p.x <= (x + width) && p.y >= y && p.y <= y + height) { return true } else { return false }
	},
	translate: (dx, dy) => {
	    x += dx
	    y += dy
	    child1.translate(dx, dy)
	    child2.translate(dx, dy)
	},
	draw: () => {
	    const panel = document.getElementById('MainPanel')
	    let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
	    let rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
	    rect.setAttribute('x', x)
	    rect.setAttribute('y', y)
	    rect.setAttribute('width', width)
	    rect.setAttribute('height', height)
	    rect.setAttribute('fill', 	'#D3D3D3')
	    svg.appendChild(rect)
	    child1.setSVG(svg)
	    child2.setSVG(svg)
	    child1.draw()
	    child2.draw()
	    panel.append(svg)
	},
	getConnectionPoint: (rx, ry) => {
	    let centerX = x + width / 2
	    let centerY = y + height / 2
	    let dx = rx - centerX
	    let dy = ry - centerY

	    if (dx < -dy && dx < dy) { return { x: centerX - (width / 2), y: centerY } } else if (dx >= dy && dx >= -dy) { return { x: centerX + (width / 2), y: centerY } } else if (dx >= -dy && dx < dy) { return { x: centerX, y: centerY + (height / 2) } } else { return { x: centerX, y: centerY - (height / 2) } }
	},
	getType: () => {
	    return 'object'
	}
    }
}

/**
 * This functions provides the childNode of the Object
 * @param {*} x x coordinate of the Child Node
 * @param {*} y y coordinate of the Child Node
 * @param {*} t text content of the Child Node
 */
function createChildNode (x, y, t) {
    let storage = t
    let ParentSVG
    return {
	setSVG: (s) => {
	    ParentSVG = s
	},
	setStorage: (s) => {
	    storage = s
	},
	getStorage: () => {
	    return storage
	},
	translate: (dx, dy) => {
	    x += dx
	    y += dy
	},
	draw: () => {
	    let text = document.createElementNS('http://www.w3.org/2000/svg', 'text')
	    const foreign = document.createElementNS('http://www.w3.org/2000/svg', "foreignObject");
	    foreign.setAttributeNS(null, 'x', x);
	    foreign.setAttributeNS(null, 'y', y);
	    foreign.setAttributeNS(null, 'width', 110);
	    foreign.setAttributeNS(null, 'height', 100);
	    const text_content= document.createElement("p");
	    text_content.textContent = storage;
	    const body = document.createElement("body");
	    body.appendChild(text_content);
	    foreign.appendChild(body);
	    ParentSVG.appendChild(foreign)
	},
	getType: () => {
	    return 'childObject'
	}
    }
}
