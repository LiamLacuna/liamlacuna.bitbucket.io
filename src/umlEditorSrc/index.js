'use strict'

/**
 * This function turns the element of the main svg to canvas, then 
 * turns the canvas as image then save as PNG
 */
function save() {
	html2canvas(document.querySelector("#capture"), {
		backgroundColor: 'transparent', 
	}).then(canvas => {
		canvasTurnImg(canvas)
	});
}

/**
 * This function would change the image type to PNG and save it locally 
 * @param {*} canvas the canvas that turned by the main svg
 */
function canvasTurnImg(canvas) {
	// set the saved image type to png
	var type = 'png';
	var imgData = canvas.toDataURL(type);
	/**
	 * Getting the mime-type
	 * @param  {String} type the old mime-type
	 * @return the new mime-type
	 */
	var _fixType = function (type) {
			type = type.toLowerCase().replace(/jpg/i, 'jpeg');
			var r = type.match(/png|jpeg|bmp|gif/)[0];
			return 'image/' + r;
	};

	/**
	 * Save the file to local
	 * @param  {String} data     the image data to stored
	 * @param  {String} filename the filename to stored
	 */
	var saveFile = function (data, filename) {
			var save_link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a');
			save_link.href = data;
			save_link.download = filename;

			var event = document.createEvent('MouseEvents');
			event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
			save_link.dispatchEvent(event);
	};

	// the filename after downloaded
	var filename = 'screenshots_card_' + (new Date()).getTime() + '.' + type;

	// download
	saveFile(imgData, filename);
}