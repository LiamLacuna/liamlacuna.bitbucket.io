'use strict'

let graphFrame
let movement = 'done' // This will store what we plan to do with stuff, ie: remove/selected/drawlines....
let selectedNode = 'NoneSelected'
'use strict'
/**
 * GraphFrame class to set the main frame and function
 */
class GraphFrame {
    /**
     * GraphFrame ctor add remove find draw method
     */
    constructor () {
	this.object = []
    }
    add (n) {
	this.object.push(n)
    }
    remove (p) {
	let ar 
	for (let i = this.object.length - 1; i >= 0; i--) {
	    const n = this.object[i]
	    if (n.contains(p)){
		this.object.splice(i, 1)
		ar = n.getlines()
	    }
	}
	for (let i = this.object.length - 1; i >= 0; i--) {
	    for (let j = ar.length - 1; j >= 0; j--){
		if(ar[j]===this.object[i]){
		    this.object.splice(i, 1) 
		}
	    }
	}
    }
    find (p) {
	for (let i = this.object.length - 1; i >= 0; i--) {
	    const n = this.object[i]
	    if (n.contains(p)){
		return n
	    }
	}
	return undefined
    }
    draw () {
	for (const n of this.object) {
	    n.draw()
	}
    }
}
/**
 * @param graphFrame
 */
function getFrame () {
    return graphFrame
}
/**
 * createCircleNode to create the circle node with coordinate and size and color
 * @param {nunmber} x componet of x 
 * @param {nunmber} y componet of y
 * @param {number} size size of node
 * @param {color} color color node
 */
function createCircleNode (x, y, size, color) // THis will create circle Node - can be modify to other object
{
    let type = "circle"
    let Color = color
    let Size = size
    let lines = []
    return {
	getBounds: () => {
            return {
		x: x,
		y: y,
		width: Size,
		height: Size,
            }
	},
	addlines:(l)=>{
	    lines.push(l)
	},
	getlines:()=>{
	    return lines
	},
	contains: p => {
            return (x + Size / 2 - p.x) ** 2 + (y + Size / 2 - p.y) ** 2 <= Size ** 2 / 4
	},
	translate: (dx, dy) => {
            x += dx
            y += dy
	},
	draw: () => {
            const panel = document.getElementById('MainPanel')
            const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
            circle.setAttribute('stroke', 'black')
            circle.setAttribute('cx', x + Size / 2)
            circle.setAttribute('cy', y + Size / 2)
            circle.setAttribute('r', Size / 2)
            circle.setAttribute('fill', Color)
            panel.appendChild(circle)
	},
	getConnectionPoint: (ox, oy) => {
            let centerX = x + Size / 2
            let centerY = y + Size / 2
            let dx = ox - centerX
            let dy = oy - centerY
            let distance = Math.sqrt(dx * dx + dy * dy)
            if (distance === 0) { return { x: ox, y: oy } } else {
		return { x: centerX + dx * (Size / 2) / distance, y: centerY + dy * (Size / 2) / distance}
            }
	},
	getType: () => {
            return type
	},
	getColor: () => {
	    return Color
	},
	setColor: (c) => {
	    Color = c
	},
	getSize: () => {
	    return Size
	},
	setSize: (s) => {
	    Size = s
	}
    }
}
/**
 * create diamond node with cordinate size and color
 * @param {number} x component of x
 * @param {number} y component of y
 * @param {number} size node size 
 * @param {color} color node color
 */
function createDiamondNode (x, y, size, color) {
    let type = 'diamond'
    let Color = color
    let Size = size
    let lines = []
    return {
	getBounds: () => {
	    return {
		x: x,
		y: y,
		width: Size,
		height: Size
	    }
	},
	addlines:(l)=>{
	    lines.push(l)
	},
	getlines:()=>{
	    return lines
	},
	contains: p => {
	    return (Math.abs(p.x - (x + Size / 2)) + Math.abs(p.y - (y + Size / 2)) <= Size / 2)
	},
	translate: (dx, dy) => {
	    x += dx
	    y += dy
	},
	draw: () => {
	    const panel = document.getElementById('MainPanel')
	    const diamond = document.createElementNS('http://www.w3.org/2000/svg', 'polygon')
	    diamond.setAttribute('stroke', 'black')
	    x = parseInt(x)
	    y = parseInt(y)
	    Size = parseInt(Size)
	    const graph = (x + Size / 2) + ',' + y + ' ' + (x + Size) + ',' + (y + Size / 2) + ' ' + (x + Size / 2) + ',' + (y + Size) + ' ' + x + ',' + (y + Size / 2) + ' '
	    diamond.setAttribute('points', graph)
	    diamond.setAttribute('fill', Color)
	    panel.appendChild(diamond)
	},
	getConnectionPoint: (ox, oy) => {
	    let centerX = x + Size / 2
	    let centerY = y + Size / 2
	    let dx = ox - centerX
	    let dy = oy - centerY
	    if (dx < -dy && dx < dy) { return { x: centerX - (Size / 2), y: centerY } }
	    else if (dx >= dy && dx >= -dy) { return { x: centerX + (Size / 2), y: centerY } }
	    else if (dx >= -dy && dx < dy) { return { x: centerX, y: centerY + (Size / 2) } }
	    else { return { x: centerX, y: centerY - (Size / 2) } }
	},
	getType: () => {
	    return type
	},
	getColor: () => {
	    return Color
	},
	setColor: (c) => {
	    Color = c
	},
	getSize: () => {
	    return Size
	},
	setSize: (s) => {
	    Size = s
	}
    }
}
/**
 * create line between two select node
 * @param {node} nodeA start node 
 * @param {node} nodeB end node
 */
function NormalLine (nodeA,nodeB,dot) {
    return {
	contains: p => {
	    return false
	},
	draw: () => {
	    let x1=nodeA.getConnectionPoint(nodeB.getBounds().x,nodeB.getBounds().y).x
	    let y1=nodeA.getConnectionPoint(nodeB.getBounds().x,nodeB.getBounds().y).y
	    let x2=nodeB.getConnectionPoint(nodeA.getBounds().x,nodeA.getBounds().y).x
	    let y2=nodeB.getConnectionPoint(nodeA.getBounds().x,nodeA.getBounds().y).y
	    const panel = document.getElementById('MainPanel')
	    const  defs = document.createElementNS('http://www.w3.org/2000/svg','defs')
	    panel.appendChild(defs)
	    const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'marker')
	    arrow.setAttribute('id','arrow')
	    arrow.setAttribute('viewBox','0 0 10 10')
	    arrow.setAttribute('refX','9')
	    arrow.setAttribute('refY','5')
	    arrow.setAttribute('markerUnits','strokeWidth')
	    arrow.setAttribute('markerWidth','15')
	    arrow.setAttribute('markerHeight','20')
	    arrow.setAttribute('orient','auto')
	    const path = document.createElementNS('http://www.w3.org/2000/svg','path')
	    arrow.appendChild(path)
	    path.setAttribute('d', 'M 0 0 L 10 5 L 0 10 z')
	    path.setAttribute('stroke', "black")
	    path.setAttribute('fill', 'none')
	    defs.appendChild(arrow)
	    const aline = document.createElementNS('http://www.w3.org/2000/svg', 'line')
	    if(dot)
		aline.setAttribute('stroke-dasharray', "2 2")
	    aline.setAttribute('stroke', "black")
	    aline.setAttribute('x1', x1)
	    aline.setAttribute('y1', y1)
	    aline.setAttribute('x2', x2)
	    aline.setAttribute('y2', y2)
	    aline.setAttributeNS(null, "marker-end", "url(#arrow)")
	    panel.appendChild(aline)
	    
	}
    }
}
/**
 * create horizontal first and vertical second line between two selected node
 * @param {node} nodeA start node
 * @param {node} nodeB end node
 */
function HVEdge (nodeA, nodeB,dot) {
    return {
	contains: p => {
	    return false
	},
	draw: () => {
	    let x1=nodeA.getConnectionPoint(nodeB.getBounds().x,nodeB.getBounds().y).x
	    let y1=nodeA.getConnectionPoint(nodeB.getBounds().x,nodeB.getBounds().y).y
	    let x2=nodeB.getConnectionPoint(nodeA.getBounds().x,nodeA.getBounds().y).x
	    let y2=nodeB.getConnectionPoint(nodeA.getBounds().x,nodeA.getBounds().y).y
	    const panel = document.getElementById('MainPanel')
	    const firstline = document.createElementNS('http://www.w3.org/2000/svg', 'line')
	    const secondline = document.createElementNS('http://www.w3.org/2000/svg', 'line')
	    const  defs = document.createElementNS('http://www.w3.org/2000/svg','defs')
	    panel.appendChild(defs)
	    const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'marker')
	    arrow.setAttribute('id','arrow')
	    arrow.setAttribute('viewBox','0 0 10 10')
	    arrow.setAttribute('refX','9')
	    arrow.setAttribute('refY','5')
	    arrow.setAttribute('markerUnits','strokeWidth')
	    arrow.setAttribute('markerWidth','15')
	    arrow.setAttribute('markerHeight','20')
	    arrow.setAttribute('orient','auto')
	    secondline.setAttributeNS(null, "marker-end", "url(#arrow)")
	    const path = document.createElementNS('http://www.w3.org/2000/svg','path')
	    arrow.appendChild(path)
	    path.setAttribute('d', 'M 0 0 L 10 5 L 0 10 z')
	    path.setAttribute('stroke', "black")
	    path.setAttribute('fill', 'none')
	    defs.appendChild(arrow)
	    if(dot)
	    {
		firstline.setAttribute('stroke-dasharray', "2 2")
		secondline.setAttribute('stroke-dasharray', "2 2")
	    }
	    firstline.setAttribute('stroke', 'black')
	    firstline.setAttribute('x1', x1)
	    firstline.setAttribute('y1', y1)
	    firstline.setAttribute('x2', x2)
	    firstline.setAttribute('y2', y1)
	    secondline.setAttribute('stroke', 'black')
	    secondline.setAttribute('x1', x2)
	    secondline.setAttribute('y1', y1)
	    secondline.setAttribute('x2', x2)
	    secondline.setAttribute('y2', y2)
	    panel.appendChild(firstline)
	    panel.appendChild(secondline)
	}
    }
}
/**
 * create vertical first and horizontal line second between two selected node
 * @param {node} nodeA start node
 * @param {node} nodeB end node
 */
function VHEdge (nodeA,nodeB,dot) {
    return {
	contains: p => {
	    return false
	},
	draw: () => {
	    let x1=nodeA.getConnectionPoint(nodeB.getBounds().x,nodeB.getBounds().y).x
	    let y1=nodeA.getConnectionPoint(nodeB.getBounds().x,nodeB.getBounds().y).y
	    let x2=nodeB.getConnectionPoint(nodeA.getBounds().x,nodeA.getBounds().y).x
	    let y2=nodeB.getConnectionPoint(nodeA.getBounds().x,nodeA.getBounds().y).y
	    const panel = document.getElementById('MainPanel')
	    const firstline = document.createElementNS('http://www.w3.org/2000/svg', 'line')
	    const secondline = document.createElementNS('http://www.w3.org/2000/svg', 'line')
	    const  defs = document.createElementNS('http://www.w3.org/2000/svg','defs')
	    panel.appendChild(defs)
	    const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'marker')
	    arrow.setAttribute('id','arrow')
	    arrow.setAttribute('viewBox','0 0 10 10')
	    arrow.setAttribute('refX','9')
	    arrow.setAttribute('refY','5')
	    arrow.setAttribute('markerUnits','strokeWidth')
	    arrow.setAttribute('markerWidth','15')
	    arrow.setAttribute('markerHeight','20')
	    arrow.setAttribute('orient','auto')
	    secondline.setAttributeNS(null, "marker-end", "url(#arrow)")
	    if(dot)
	    {
		firstline.setAttribute('stroke-dasharray', "2 2")
		secondline.setAttribute('stroke-dasharray', "2 2")
	    }
	    firstline.setAttribute('stroke', 'black')
	    firstline.setAttribute('x1', x1)
	    firstline.setAttribute('y1', y1)
	    firstline.setAttribute('x2', x1)
	    firstline.setAttribute('y2', y2)
	    secondline.setAttribute('stroke', 'black')
	    secondline.setAttribute('x1', x1)
	    secondline.setAttribute('y1', y2)
	    secondline.setAttribute('x2', x2)
	    secondline.setAttribute('y2', y2)
	    panel.appendChild(firstline)
	    panel.appendChild(secondline)
	}
    }
}
/**
 * create curve line between two node
 * @param {node} nodeA start node
 * @param {node} nodeB end node
 */
function CurveEdge(nodeA,nodeB,dot){
    return {
	contains: p => {
	    return false
	},
	draw:()=>{
	    let x1=nodeA.getConnectionPoint(nodeB.getBounds().x,nodeB.getBounds().y).x
	    let y1=nodeA.getConnectionPoint(nodeB.getBounds().x,nodeB.getBounds().y).y
	    let x2=nodeB.getConnectionPoint(nodeA.getBounds().x,nodeA.getBounds().y).x
	    let y2=nodeB.getConnectionPoint(nodeA.getBounds().x,nodeA.getBounds().y).y
	    const panel = document.getElementById('MainPanel')
	    const curve1 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
	    const curve2 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
	    const  defs = document.createElementNS('http://www.w3.org/2000/svg','defs')
	    panel.appendChild(defs)
	    const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'marker')
	    arrow.setAttribute('id','arrow')
	    arrow.setAttribute('viewBox','0 0 10 10')
	    arrow.setAttribute('refX','9')
	    arrow.setAttribute('refY','5')
	    arrow.setAttribute('markerUnits','strokeWidth')
	    arrow.setAttribute('markerWidth','15')
	    arrow.setAttribute('markerHeight','20')
	    arrow.setAttribute('orient','auto')
	    curve2.setAttributeNS(null, "marker-end", "url(#arrow)")
	    if(dot)
	    {
		curve1.setAttribute('stroke-dasharray', "2 2")
		curve2.setAttribute('stroke-dasharray', "2 2")
	    }
	    var mx = (x2 + x1)/2
	    var my = (y2 + y1)/2
	    var cur1 ='M'+x1+' '+y1+ 'Q'+' '+mx+' '+y1 +' '+mx+' '+my
	    var cur2 ='M'+mx+' '+my+ 'Q'+' '+mx+' '+y2 +' '+x2+' '+y2
	    curve1.setAttribute('d', cur1)
	    curve1.setAttribute('fill', 'transparent' )
	    curve1.setAttribute('stroke', 'black')
	    curve2.setAttribute('d', cur2)
	    curve2.setAttribute('fill', 'transparent' )
	    curve2.setAttribute('stroke', 'black')
	    panel.appendChild(curve1)
	    panel.appendChild(curve2)
	}
    }
}
/**
 * check the style and send to differnet line
 * @param {node} nodeA start node
 * @param {node} nodeB end node
 * @param {name} style style name
 */
function addline (nodeA, nodeB, style) {
    if (style === 'line') {
	let temp = NormalLine(nodeA,nodeB,false) 
	graphFrame.add(temp)
	nodeA.addlines(temp)
	nodeB.addlines(temp)
    }else if (style === 'dotline') {
	let temp = NormalLine(nodeA,nodeB,true)
	graphFrame.add(temp)
	nodeA.addlines(temp)
	nodeB.addlines(temp)
    }else if (style === 'VHEdge') {
	let temp = VHEdge(nodeA,nodeB,false)
	graphFrame.add(temp)
	nodeA.addlines(temp)
	nodeB.addlines(temp)
    }else if (style === 'dotVHEdge') {
	let temp = VHEdge(nodeA,nodeB,true)
	graphFrame.add(temp)
	nodeA.addlines(temp)
	nodeB.addlines(temp)
    } else if (style === 'HVEdge') {
	let temp = HVEdge(nodeA,nodeB,false)
	graphFrame.add(temp)
	nodeA.addlines(temp)
	nodeB.addlines(temp)
    }else if (style === 'dotHVEdge') {
	let temp = HVEdge(nodeA,nodeB,true)
	graphFrame.add(temp)
	nodeA.addlines(temp)
	nodeB.addlines(temp)
    }else if (style === 'CurveEdge') {
	let temp = CurveEdge(nodeA,nodeB,false)
	graphFrame.add(temp)
	nodeA.addlines(temp)
	nodeB.addlines(temp)
    }else if (style === 'dotCurveEdge') {
	let temp = CurveEdge(nodeA,nodeB,true)
	graphFrame.add(temp)
	nodeA.addlines(temp)
	nodeB.addlines(temp)
    }
    
}
/**
 * create node
 * @param {node} node node
 * @param {color} color color
 */
function addNode (node, color) {
    movement = 'Node'
    if (node === 'circle' && color === 'white') {
	graphFrame.add(createCircleNode(10, 10, 20, 'white'))
    } else if (node === 'circle' && color === 'black') {
	graphFrame.add(createCircleNode(10, 10, 20, 'black'))
    } else if (node === 'diamond' && color === 'white') {
	graphFrame.add(createDiamondNode(10, 10, 20, 'white'))
    } else if (node === 'object') {
	graphFrame.add(createObjectNode(Math.floor(Math.random() * 400), Math.floor(Math.random() * 200)))
    }
    graphFrame.draw()
    movement = 'done'
}
/**
 * grab the node been clicked
 * @param {number} x x coordinate 
 * @param {number} y y coordinate
 */
function drawGrabber (x, y) // This will grab any Node that have been clicked - can be modify to use for trying to move component around
{
    const size = 5
    const panel = document.getElementById('MainPanel')
    const square = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    square.setAttribute('x', x - size / 2)
    square.setAttribute('y', y - size / 2)
    square.setAttribute('width', size)
    square.setAttribute('height', size)
    square.setAttribute('fill', 'black')
    panel.appendChild(square)
}
/**
 * get the function to use
 * @param {string} name function name
 */
function chooseFunction (name) {
    movement = name
}
/**
 * create the button
 * @param {number} x botton coordinate
 * @param {string} name button name
 * @param {function} functions button function
 */
function createButton (x, name, functions) {
    const size = 50
    const panel = document.getElementById('toolbar')
    const square = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    square.setAttribute('stroke', 'black')
    square.setAttribute('x', x)
    square.setAttribute('y', 0)
    square.setAttribute('width', size)
    square.setAttribute('height', size)
    square.setAttribute('fill', 'white')
    square.addEventListener('click', functions)
    panel.appendChild(square)
    drawButton(x + 5, name, functions)
}
/**
 * draw button
 * @param {number} x botton cordinate 
 * @param {string} name button name
 * @param {function} functions button function
 */
function drawButton (x, name, functions) {
    const size = 40
    const y = 5
    if (name === 'circleW') {
	const panel = document.getElementById('toolbar')
	const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
	circle.setAttribute('stroke', 'black')
	circle.setAttribute('cx', x + size / 2)
	circle.setAttribute('cy', y + size / 2)
	circle.setAttribute('r', size / 2)
	circle.setAttribute('fill', 'white')
	circle.addEventListener('click', functions)
	panel.appendChild(circle)
    } else if (name === 'circleB') {
	const panel = document.getElementById('toolbar')
	const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
	circle.setAttribute('stroke', 'black')
	circle.setAttribute('cx', x + size / 2)
	circle.setAttribute('cy', y + size / 2)
	circle.setAttribute('r', size / 2)
	circle.setAttribute('fill', 'black')
	circle.addEventListener('click', functions)
	panel.appendChild(circle)
    } else if (name === 'diamond') {
	const panel = document.getElementById('toolbar')
	const diamond = document.createElementNS('http://www.w3.org/2000/svg', 'polygon')
	diamond.setAttribute('stroke', 'black')
	const graph = (x + 20) + ',' + (y + 1) + ' ' + (x + 39) + ',' + (y + 20) + ' ' + (x + 20) + ',' + (y + 39) + ' ' + (x + 1) + ',' + (y + 20) + ' '
	diamond.setAttribute('points', graph)
	diamond.setAttribute('fill', 'white')
	diamond.addEventListener('click', functions)
	panel.appendChild(diamond)
    } else if (name === 'line'||name === 'dotline') {
	const panel = document.getElementById('toolbar')
	const line = document.createElementNS('http://www.w3.org/2000/svg', 'line')
	if(name === 'dotline')
	    line.setAttribute('stroke-dasharray', "2 2")
	line.setAttribute('stroke', 'black')
	line.setAttribute('x1', x + 5)
	line.setAttribute('y1', y + 5)
	line.setAttribute('x2', x + 35)
	line.setAttribute('y2', y + 35)
	line.addEventListener('click', functions)
	panel.appendChild(line)
    }else if (name === 'HVEdge'||name === 'dotHVEdge') {
	const panel = document.getElementById('toolbar')
	const line = document.createElementNS('http://www.w3.org/2000/svg', 'polyline')
	if(name === 'dotHVEdge')
	    line.setAttribute('stroke-dasharray', "2 2")
	line.setAttribute('stroke', 'black')
	const graph = (x + 5) + ',' + (y + 5) + ' ' + (x + 35) + ',' + (y + 5) + ' ' + (x + 35) + ',' + (y + 35)
	line.setAttribute('points', graph)
	line.setAttribute('fill', 'none')
	line.addEventListener('click', functions)
	panel.appendChild(line)
    } else if (name === 'VHEdge'||name === 'dotVHEdge') {
	const panel = document.getElementById('toolbar')
	const line = document.createElementNS('http://www.w3.org/2000/svg', 'polyline')
	if(name === 'dotVHEdge')
	    line.setAttribute('stroke-dasharray', "2 2")
	line.setAttribute('stroke', 'black')
	const graph = (x + 5) + ',' + (y + 5) + ' ' + (x + 5) + ',' + (y + 35) + ' ' + (x + 35) + ',' + (y + 35)
	line.setAttribute('points', graph)
	line.setAttribute('fill', 'none')
	line.addEventListener('click', functions)
	panel.appendChild(line)
    } else if (name === 'CurveEdge'||name === 'dotCurveEdge') {
	const panel = document.getElementById('toolbar')
	const curve = document.createElementNS('http://www.w3.org/2000/svg', 'path')
	if(name === 'dotCurveEdge')
	    curve.setAttribute('stroke-dasharray', "2 2")
	var x1=x+20
	var y1=y+10
	var x2=x1+10
	var y2 =y2+10
	var cur ='M' + x +' '+ y+ 'C' + x1 +' '+ y1 + ',' + x2 +' '+ y1 + ',' + (x+40) +' '+ (y+40)
	curve.setAttribute('d', cur)
	curve.setAttribute('fill', 'transparent' )
	curve.setAttribute('stroke', 'black')
	curve.addEventListener('click', functions)
	panel.appendChild(curve)
    } else if (name === 'select') {
	const panel = document.getElementById('toolbar')
	const square1 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
	const square2 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
	const square3 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
	const square4 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
	square1.setAttribute('x', x + 1)
	square1.setAttribute('y', y + 1)
	square1.setAttribute('width', 5)
	square1.setAttribute('height', 5)
	square1.addEventListener('click', functions)
	panel.appendChild(square1)
	square2.setAttribute('x', x + 34)
	square2.setAttribute('y', y + 1)
	square2.setAttribute('width', 5)
	square2.setAttribute('height', 5)
	square2.addEventListener('click', functions)
	panel.appendChild(square2)
	square3.setAttribute('x', x + 34)
	square3.setAttribute('y', y + 34)
	square3.setAttribute('width', 5)
	square3.setAttribute('height', 5)
	square3.addEventListener('click', functions)
	panel.appendChild(square3)
	square4.setAttribute('x', x + 1)
	square4.setAttribute('y', y + 34)
	square4.setAttribute('width', 5)
	square4.setAttribute('height', 5)
	square4.addEventListener('click', functions)
	panel.appendChild(square4)
    } else if (name === 'Delete') {
	const panel = document.getElementById('toolbar')
	const remove = document.createElementNS('http://www.w3.org/2000/svg', 'text')
	remove.setAttribute('x', x - 5)
	remove.setAttribute('y', y + 27)
	remove.setAttribute('style', 'cursor: pointer;')
	const txt = document.createTextNode('Delete')
	remove.appendChild(txt)
	remove.addEventListener('click', functions)
	panel.appendChild(remove)
    } else if (name === 'object') {
	const panel = document.getElementById('toolbar')
	const addObject = document.createElementNS('http://www.w3.org/2000/svg', 'text')
	addObject.setAttribute('x', x - 5)
	addObject.setAttribute('y', y + 27)
	addObject.setAttribute('style', 'cursor: pointer;')
	const txt = document.createTextNode('Object')
	addObject.appendChild(txt)
	addObject.addEventListener('click', functions)
	panel.appendChild(addObject)
    }
}
/**
 * create the tool bar
 */
function createToolbar () {
    createButton(0, 'circleW', function () { addNode('circle', 'white') })
    createButton(50, 'circleB', function () { addNode('circle', 'black') })
    createButton(100, 'diamond', function () { addNode('diamond', 'white') })
    createButton(150, 'line', function () { chooseFunction('line') })
    createButton(200, 'dotline', function () { chooseFunction('dotline') })
    createButton(250, 'HVEdge', function () { chooseFunction('HVEdge') })
    createButton(300, 'dotHVEdge', function () { chooseFunction('dotHVEdge') })
    createButton(350, 'VHEdge', function () { chooseFunction('VHEdge') })
    createButton(400, 'dotVHEdge', function () { chooseFunction('dotVHEdge') })
    createButton(450, 'CurveEdge', function () { chooseFunction('CurveEdge') })
    createButton(500, 'dotCurveEdge', function () { chooseFunction('dotCurveEdge') })
    createButton(550, 'select', function () { chooseFunction('selected') })
    createButton(600, 'Delete', function () { chooseFunction('Delete') })
    createButton(650, 'object', function () { addNode('object') })
}
/**
 * eventlistener 
 */
document.addEventListener('DOMContentLoaded', function () {
    graphFrame = new GraphFrame()
    const panel = document.getElementById('MainPanel')
    let selected
    let dragStartPoint
    let dragStartBounds
    createToolbar()
    /**
     * repaint method to repaint
     */
    function repaint () {
	panel.innerHTML = ''
	graphFrame.draw()
	if (selected !== undefined) {
	    const bounds = selected.getBounds()
	    bounds.x = parseInt(bounds.x)
            bounds.y = parseInt(bounds.y)
            bounds.width = parseInt(bounds.width)
            bounds.height = parseInt(bounds.height)

            drawGrabber(bounds.x, bounds.y)
            drawGrabber(bounds.x + bounds.width, bounds.y)
            drawGrabber(bounds.x, bounds.y + bounds.height)
            drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
	}
    }
    /**
     * get the mouse location
     * @param {} event 
     */
    function mouseLocation (event) {
	var rect = panel.getBoundingClientRect()
	return {
            x: event.clientX - rect.left,
            y: event.clientY - rect.top
	}
    }

    panel.addEventListener('mousedown', event => {
	let mousePoint = mouseLocation(event) 
	
	selected = graphFrame.find(mousePoint)
	if (selected !== undefined) {
	    if (selectedNode != 'NoneSelected') {
		if (movement === 'line') { addline(selected, selectedNode, 'line') }
		else if (movement === 'dotline') { addline(selected, selectedNode, 'dotline') }
		else if (movement === 'HVEdge') { addline(selected, selectedNode, 'HVEdge') }
		else if (movement === 'dotHVEdge') { addline(selected, selectedNode, 'dotHVEdge') }
		else if (movement === 'VHEdge') { addline(selected, selectedNode, 'VHEdge') }
		else if (movement === 'dotVHEdge') { addline(selected, selectedNode, 'dotVHEdge') }
		else if (movement === 'CurveEdge') { addline(selected, selectedNode, 'CurveEdge') }
		else if (movement === 'dotCurveEdge') { addline(selected, selectedNode, 'dotCurveEdge') }
		movement = 'done'
		selectedNode = 'NoneSelected'
	    } else {
		if (movement === 'Delete'){
		    graphFrame.remove(mousePoint)
		}
		else if (movement === 'selected') {
		    selectedNode = selected
		    const panel = document.getElementById('propertySheet')

		    if(selectedNode.getType() === 'object') {
			// let isExited = panel.classList.contains('object')
			let child = selectedNode.getChild1()
			let child2 = selectedNode.getChild2()
			openObjectPropertySheet(child, child2, selectedNode)
		    }
		    else if(selectedNode.getType() === 'circle' || selectedNode.getType() === 'diamond') 
		    {
			let node = selectedNode
			openNodePropertySheet(node, selectedNode)
		    }
		}
		else {
		    movement = 'done'
		    selectedNode = 'NoneSelected'
		}
	    }
	    dragStartPoint = mousePoint
	    dragStartBounds = selected.getBounds()
	}
	else {
	    movement = 'done'
	    selectedNode = 'NoneSelected'
	}
	repaint()
    })

    panel.addEventListener('mousemove', event => {
	if (movement != 'line' && movement != 'HVEdge' && movement != 'VHEdge' && movement != 'CurveEdge' && movement != 'selected') {
	    if (dragStartPoint === undefined) return
	    let mousePoint = mouseLocation(event)
	    if (selected !== undefined) {
		const bounds = selected.getBounds()
		selected.translate(
		    dragStartBounds.x - bounds.x + mousePoint.x - dragStartPoint.x,
		    dragStartBounds.y - bounds.y + mousePoint.y - dragStartPoint.y
		)
		repaint()
	    }
	}
    })

    panel.addEventListener('mouseup', event => {
	dragStartPoint = undefined
	dragStartBounds = undefined
    })


    function openObjectPropertySheet (child, child2, selected) {
	const panel = document.getElementById('propertySheet')
	panel.classList.add('object')
	const form = document.createElement('form')
	let input = document.createElement('input')
	let input2 = document.createElement('input')
	const label = document.createElement('label')
	const label2 = document.createElement('label')
	const button = document.createElement('button')
	const lineBreak = document.createElement("br");
	
	
	label.innerHTML = "Object Name:  "
	label2.innerHTML = "Attributes Name: "
	button.innerHTML = "Submit the change" 
	
	button.type = "button"
	form.appendChild(label)
	form.appendChild(input)
	form.appendChild(lineBreak)
	form.appendChild(label2)
	form.appendChild(input2)
	form.appendChild(button)
	panel.appendChild(form)
	input.type = "text";
	input2.type = "text"
	
	button.addEventListener("click", () => {
	    child.setStorage(input.value)
	    child2.setStorage(input2.value)
	    selected.draw()
	    panel.removeChild(form);
	})
    }
    /**
     * open the node property sheet
     * @param {node} node selected node and open property sheet
     */
    function openNodePropertySheet (node) {
	const panel = document.getElementById('propertySheet')
	panel.classList.add('node')
	const form = document.createElement('form')
	let input1 = document.createElement('input')
	let input2 = document.createElement('input')
	const label1 = document.createElement('label')
	const label2 = document.createElement('label')
	const button1 = document.createElement('button')
	const button2 = document.createElement('button')
	
	label1.innerHTML = "Color:  "
	label2.innerHTML = "Size: "
	button1.innerHTML = "Change Color" 
	button2.innerHTML = "Change Size" 
	button1.type = "button"
	button2.type = "button"
	form.appendChild(label1)
	form.appendChild(label2)
	form.appendChild(input1)
	form.appendChild(input2)
	form.appendChild(button1)
	form.appendChild(button2)
	panel.appendChild(form)
	input1.type = "text";
	input1.value = node.getColor();
	input2.type = "integer";
	input2.value = node.getSize();
	form.classList.add("new_form");
	
	button1.addEventListener("click", () => {
	    node.setColor(input1.value)
	    repaint()
	    
	})
	
	button2.addEventListener("click", () => {
	    node.setSize(input2.value)

	    repaint()
	})

    }
    /**
     * close the property sheet
     */
    function closePropertySheet() {
	const panel = document.getElementById('propertySheet')
	document.getElementById("propertySheet").style.display='none';
    }
})
