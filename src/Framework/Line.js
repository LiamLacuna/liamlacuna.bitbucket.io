/**
 * create line between two select node
 * @param {node} nodeA start node
 * @param {node} nodeB end node
 */
function NormalLine (nodeA, nodeB, dot) {
  return {
    contains: p => {
      return false
    },
    draw: () => {
      let x1 = nodeA.getConnectionPoint(nodeB.getBounds().x, nodeB.getBounds().y).x
      let y1 = nodeA.getConnectionPoint(nodeB.getBounds().x, nodeB.getBounds().y).y
      let x2 = nodeB.getConnectionPoint(nodeA.getBounds().x, nodeA.getBounds().y).x
      let y2 = nodeB.getConnectionPoint(nodeA.getBounds().x, nodeA.getBounds().y).y
      const panel = document.getElementById('MainPanel')
      const defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs')
      panel.appendChild(defs)
      const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'marker')
      defs.appendChild(arrow)
      const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
      arrow.appendChild(path)
      arrow.setAttribute('id', 'arrow')
      arrow.setAttribute('viewBox', '0 0 10 10')
      arrow.setAttribute('refX', '9')
      arrow.setAttribute('refY', '5')
      arrow.setAttribute('markerUnits', 'strokeWidth')
      arrow.setAttribute('markerWidth', '15')
      arrow.setAttribute('markerHeight', '20')
      arrow.setAttribute('orient', 'auto')
      path.setAttribute('d', 'M 0 0 L 10 5 L 0 10 z')
      path.setAttribute('stroke', 'black')
      path.setAttribute('fill', 'none')
      const aline = document.createElementNS('http://www.w3.org/2000/svg', 'line')
      aline.setAttributeNS(null, 'marker-end', 'url(#arrow)')
      if (dot) { aline.setAttribute('stroke-dasharray', '2 2') }
      aline.setAttribute('stroke', 'black')
      aline.setAttribute('x1', x1)
      aline.setAttribute('y1', y1)
      aline.setAttribute('x2', x2)
      aline.setAttribute('y2', y2)
      panel.appendChild(aline)
    }
  }
}

/**
 * create horizontal first and vertical second line between two selected node
 * @param {node} nodeA start node
 * @param {node} nodeB end node
 */
function HVEdge (nodeA, nodeB, dot) {
  return {
    contains: p => {
      return false
    },
    draw: () => {
      let x1 = nodeA.getConnectionPoint(nodeB.getBounds().x, nodeB.getBounds().y).x
      let y1 = nodeA.getConnectionPoint(nodeB.getBounds().x, nodeB.getBounds().y).y
      let x2 = nodeB.getConnectionPoint(nodeA.getBounds().x, nodeA.getBounds().y).x
      let y2 = nodeB.getConnectionPoint(nodeA.getBounds().x, nodeA.getBounds().y).y
      const panel = document.getElementById('MainPanel')
      const firstline = document.createElementNS('http://www.w3.org/2000/svg', 'line')
      const secondline = document.createElementNS('http://www.w3.org/2000/svg', 'line')
      const defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs')
      panel.appendChild(defs)
      const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'marker')
      arrow.setAttribute('id', 'arrow')
      arrow.setAttribute('viewBox', '0 0 10 10')
      arrow.setAttribute('refX', '9')
      arrow.setAttribute('refY', '5')
      arrow.setAttribute('markerUnits', 'strokeWidth')
      arrow.setAttribute('markerWidth', '15')
      arrow.setAttribute('markerHeight', '20')
      arrow.setAttribute('orient', 'auto')
      const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
      arrow.appendChild(path)
      path.setAttribute('d', 'M 0 0 L 10 5 L 0 10 z')
      path.setAttribute('stroke', 'black')
      path.setAttribute('fill', 'none')
      defs.appendChild(arrow)
      secondline.setAttributeNS(null, 'marker-end', 'url(#arrow)')
      if (dot) {
        firstline.setAttribute('stroke-dasharray', '2 2')
        secondline.setAttribute('stroke-dasharray', '2 2')
      }
      firstline.setAttribute('stroke', 'black')
      firstline.setAttribute('x1', x1)
      firstline.setAttribute('y1', y1)
      firstline.setAttribute('x2', x2)
      firstline.setAttribute('y2', y1)
      secondline.setAttribute('stroke', 'black')
      secondline.setAttribute('x1', x2)
      secondline.setAttribute('y1', y1)
      secondline.setAttribute('x2', x2)
      secondline.setAttribute('y2', y2)
      panel.appendChild(firstline)
      panel.appendChild(secondline)
    }
  }
}
/**
 * create vertical first and horizontal line second between two selected node
 * @param {node} nodeA start node
 * @param {node} nodeB end node
 */
function VHEdge (nodeA, nodeB, dot) {
  return {
    contains: p => {
      return false
    },
    draw: () => {
      let x1 = nodeA.getConnectionPoint(nodeB.getBounds().x, nodeB.getBounds().y).x
      let y1 = nodeA.getConnectionPoint(nodeB.getBounds().x, nodeB.getBounds().y).y
      let x2 = nodeB.getConnectionPoint(nodeA.getBounds().x, nodeA.getBounds().y).x
      let y2 = nodeB.getConnectionPoint(nodeA.getBounds().x, nodeA.getBounds().y).y
      const panel = document.getElementById('MainPanel')
      const firstline = document.createElementNS('http://www.w3.org/2000/svg', 'line')
      const secondline = document.createElementNS('http://www.w3.org/2000/svg', 'line')
      const defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs')
      panel.appendChild(defs)
      const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'marker')
      defs.appendChild(arrow)
      const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
      arrow.appendChild(path)
      arrow.setAttribute('id', 'arrow')
      arrow.setAttribute('viewBox', '0 0 10 10')
      arrow.setAttribute('refX', '9')
      arrow.setAttribute('refY', '5')
      arrow.setAttribute('markerUnits', 'strokeWidth')
      arrow.setAttribute('markerWidth', '15')
      arrow.setAttribute('markerHeight', '20')
      arrow.setAttribute('orient', 'auto')
      path.setAttribute('d', 'M 0 0 L 10 5 L 0 10 z')
      path.setAttribute('stroke', 'black')
      path.setAttribute('fill', 'none')
      secondline.setAttributeNS(null, 'marker-end', 'url(#arrow)')
      if (dot) {
        firstline.setAttribute('stroke-dasharray', '2 2')
        secondline.setAttribute('stroke-dasharray', '2 2')
      }
      firstline.setAttribute('stroke', 'black')
      firstline.setAttribute('x1', x1)
      firstline.setAttribute('y1', y1)
      firstline.setAttribute('x2', x1)
      firstline.setAttribute('y2', y2)
      secondline.setAttribute('stroke', 'black')
      secondline.setAttribute('x1', x1)
      secondline.setAttribute('y1', y2)
      secondline.setAttribute('x2', x2)
      secondline.setAttribute('y2', y2)
      panel.appendChild(firstline)
      panel.appendChild(secondline)
    }
  }
}

/**
 * create curve line between two node
 * @param {node} nodeA start node
 * @param {node} nodeB end node
 */
function CurveEdge (nodeA, nodeB, dot) {
  return {
    contains: p => {
      return false
    },
    draw: () => {
      let x1 = nodeA.getConnectionPoint(nodeB.getBounds().x, nodeB.getBounds().y).x
      let y1 = nodeA.getConnectionPoint(nodeB.getBounds().x, nodeB.getBounds().y).y
      let x2 = nodeB.getConnectionPoint(nodeA.getBounds().x, nodeA.getBounds().y).x
      let y2 = nodeB.getConnectionPoint(nodeA.getBounds().x, nodeA.getBounds().y).y
      const panel = document.getElementById('MainPanel')
      const curve1 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
      const curve2 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
      const defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs')
      panel.appendChild(defs)
      const arrow = document.createElementNS('http://www.w3.org/2000/svg', 'marker')
      defs.appendChild(arrow)
      const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
      arrow.appendChild(path)
      arrow.setAttribute('id', 'arrow')
      arrow.setAttribute('viewBox', '0 0 10 10')
      arrow.setAttribute('refX', '9')
      arrow.setAttribute('refY', '5')
      arrow.setAttribute('markerUnits', 'strokeWidth')
      arrow.setAttribute('markerWidth', '15')
      arrow.setAttribute('markerHeight', '20')
      arrow.setAttribute('orient', 'auto')
      path.setAttribute('d', 'M 0 0 L 10 5 L 0 10 z')
      path.setAttribute('stroke', 'black')
      path.setAttribute('fill', 'none')
      curve2.setAttributeNS(null, 'marker-end', 'url(#arrow)')
      if (dot) {
        curve1.setAttribute('stroke-dasharray', '2 2')
        curve2.setAttribute('stroke-dasharray', '2 2')
      }
      var mx = (x2 + x1) / 2
      var my = (y2 + y1) / 2
      var cur1 = 'M' + x1 + ' ' + y1 + 'Q' + ' ' + mx + ' ' + y1 + ' ' + mx + ' ' + my
      var cur2 = 'M' + mx + ' ' + my + 'Q' + ' ' + mx + ' ' + y2 + ' ' + x2 + ' ' + y2
      curve1.setAttribute('d', cur1)
      curve1.setAttribute('fill', 'transparent')
      curve1.setAttribute('stroke', 'black')
      curve2.setAttribute('d', cur2)
      curve2.setAttribute('fill', 'transparent')
      curve2.setAttribute('stroke', 'black')
      panel.appendChild(curve1)
      panel.appendChild(curve2)
    }
  }
}
