'use strict'

/**
 * createCircleNode to create the circle node with coordinate and size and color
 * @param {number} x x coordinate of Circle Node (top left)
 * @param {number} y y coordinate of Circle Node (top left)
 * @param {number} size size of node
 * @param {color} color color node
 */
function createCircleNode (x, y, size, color) // THis will create circle Node - can be modify to other object
{
  let type = 'circle'
  let Color = color
  let Size = size
  let lines = []
  return {
    getBounds: () => {
      return {
        x: x,
        y: y,
        width: Size,
        height: Size
      }
    },
    addlines: (l) => {
	    lines.push(l)
    },
    getlines: () => {
	    return lines
    },
    contains: p => {
      return (x + Size / 2 - p.x) ** 2 + (y + Size / 2 - p.y) ** 2 <= Size ** 2 / 4
    },
    translate: (dx, dy) => {
      x += dx
      y += dy
    },
    draw: () => {
      const panel = document.getElementById('MainPanel')
      const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
      circle.setAttribute('stroke', 'black')
      circle.setAttribute('cx', x + Size / 2)
      circle.setAttribute('cy', y + Size / 2)
      circle.setAttribute('r', Size / 2)
      circle.setAttribute('fill', Color)
      panel.appendChild(circle)
    },
    getConnectionPoint: (ox, oy) => {
      let centerX = x + Size / 2
      let centerY = y + Size / 2
      let dx = ox - centerX
      let dy = oy - centerY
      let distance = Math.sqrt(dx * dx + dy * dy)
      if (distance === 0) { return { x: ox, y: oy } } else {
        return { x: centerX + dx * (Size / 2) / distance, y: centerY + dy * (Size / 2) / distance }
      }
    },
    getType: () => {
      return type
    },
    getColor: () => {
	    return Color
    },
    setColor: (c) => {
	    Color = c
    },
    getSize: () => {
	    return Size
    },
    setSize: (s) => {
	    Size = s
    }
  }
}
