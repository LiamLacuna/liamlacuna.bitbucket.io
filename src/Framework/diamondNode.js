'use strict'

/**
 * createDiamondNode to create the Diamond node with coordinate and size and color
 * @param {number} x x coordinate of Diamond Node (top left)
 * @param {number} y y coordinate of Diamond Node (top left)
 * @param {number} size size of node
 * @param {color} color color node
 */
function createDiamondNode (x, y, size, color) {
  let type = 'diamond'
  let Color = color
  let Size = size
  let lines = []
  return {
    getBounds: () => {
      return {
        x: x,
        y: y,
        width: Size,
        height: Size
      }
    },
    addlines: (l) => {
      lines.push(l)
    },
    getlines: () => {
      return lines
    },
    contains: p => {
      return (Math.abs(p.x - (x + Size / 2)) + Math.abs(p.y - (y + Size / 2)) <= Size / 2)
    },
    translate: (dx, dy) => {
      x += dx
      y += dy
    },
    draw: () => {
      const panel = document.getElementById('MainPanel')
      const diamond = document.createElementNS('http://www.w3.org/2000/svg', 'polygon')
      diamond.setAttribute('stroke', 'black')
      x = parseInt(x)
      y = parseInt(y)
      Size = parseInt(Size)
      const graph = (x + Size / 2) + ',' + y + ' ' + (x + Size) + ',' + (y + Size / 2) + ' ' + (x + Size / 2) + ',' + (y + Size) + ' ' + x + ',' + (y + Size / 2) + ' '
      diamond.setAttribute('points', graph)
      diamond.setAttribute('fill', Color)
      panel.appendChild(diamond)
    },
    getConnectionPoint: (ox, oy) => {
      let centerX = x + Size / 2
      let centerY = y + Size / 2
      let dx = ox - centerX
      let dy = oy - centerY
      if (dx < -dy && dx < dy) { return { x: centerX - (Size / 2), y: centerY } } else if (dx >= dy && dx >= -dy) { return { x: centerX + (Size / 2), y: centerY } } else if (dx >= -dy && dx < dy) { return { x: centerX, y: centerY + (Size / 2) } } else { return { x: centerX, y: centerY - (Size / 2) } }
    },
    getType: () => {
      return type
    },
    getColor: () => {
      return Color
    },
    setColor: (c) => {
      Color = c
    },
    getSize: () => {
      return Size
    },
    setSize: (s) => {
      Size = s
    }
  }
}
