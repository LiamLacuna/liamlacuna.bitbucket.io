/**
 * create the outside box of button
 * @param {number} x x coordinate of button 
 * @param {string} name the name of the button 
 * @param {function} functions function of the button
 */
function createButton (x, name, functions) {
  const size = 50
  const panel = document.getElementById('toolbar')
  const square = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
  square.setAttribute('stroke', 'black')
  square.setAttribute('x', x)
  square.setAttribute('y', 0)
  square.setAttribute('width', size)
  square.setAttribute('height', size)
  square.setAttribute('fill', 'white')
  square.addEventListener('click', functions)
  panel.appendChild(square)
  drawButton(x + 5, name, functions)
}

/**
 * draw button icons inside the box
 * @param {number} x x coordinate of button 
 * @param {string} name the name of the button 
 * @param {function} functions function of the button
 */
function drawButton (x, name, functions) {
  const size = 40
  const y = 5
  if (name === 'circleW') {
    const panel = document.getElementById('toolbar')
    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
    circle.setAttribute('stroke', 'black')
    circle.setAttribute('cx', x + size / 2)
    circle.setAttribute('cy', y + size / 2)
    circle.setAttribute('r', size / 2)
    circle.setAttribute('fill', 'white')
    circle.addEventListener('click', functions)
    panel.appendChild(circle)
  } else if (name === 'circleB') {
    const panel = document.getElementById('toolbar')
    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
    circle.setAttribute('stroke', 'black')
    circle.setAttribute('cx', x + size / 2)
    circle.setAttribute('cy', y + size / 2)
    circle.setAttribute('r', size / 2)
    circle.setAttribute('fill', 'black')
    circle.addEventListener('click', functions)
    panel.appendChild(circle)
  } else if (name === 'diamond') {
    const panel = document.getElementById('toolbar')
    const diamond = document.createElementNS('http://www.w3.org/2000/svg', 'polygon')
    diamond.setAttribute('stroke', 'black')
    const graph = (x + 20) + ',' + (y + 1) + ' ' + (x + 39) + ',' + (y + 20) + ' ' + (x + 20) + ',' + (y + 39) + ' ' + (x + 1) + ',' + (y + 20) + ' '
    diamond.setAttribute('points', graph)
    diamond.setAttribute('fill', 'white')
    diamond.addEventListener('click', functions)
    panel.appendChild(diamond)
  } else if (name === 'line' || name === 'dotline') {
    const panel = document.getElementById('toolbar')
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'line')
    if (name === 'dotline') { line.setAttribute('stroke-dasharray', '2 2') }
    line.setAttribute('stroke', 'black')
    line.setAttribute('x1', x + 5)
    line.setAttribute('y1', y + 5)
    line.setAttribute('x2', x + 35)
    line.setAttribute('y2', y + 35)
    line.addEventListener('click', functions)
    panel.appendChild(line)
  } else if (name === 'HVEdge' || name === 'dotHVEdge') {
    const panel = document.getElementById('toolbar')
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'polyline')
    if (name === 'dotHVEdge') { line.setAttribute('stroke-dasharray', '2 2') }
    line.setAttribute('stroke', 'black')
    const graph = (x + 5) + ',' + (y + 5) + ' ' + (x + 35) + ',' + (y + 5) + ' ' + (x + 35) + ',' + (y + 35)
    line.setAttribute('points', graph)
    line.setAttribute('fill', 'none')
    line.addEventListener('click', functions)
    panel.appendChild(line)
  } else if (name === 'VHEdge' || name === 'dotVHEdge') {
    const panel = document.getElementById('toolbar')
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'polyline')
    if (name === 'dotVHEdge') { line.setAttribute('stroke-dasharray', '2 2') }
    line.setAttribute('stroke', 'black')
    const graph = (x + 5) + ',' + (y + 5) + ' ' + (x + 5) + ',' + (y + 35) + ' ' + (x + 35) + ',' + (y + 35)
    line.setAttribute('points', graph)
    line.setAttribute('fill', 'none')
    line.addEventListener('click', functions)
    panel.appendChild(line)
  } else if (name === 'CurveEdge' || name === 'dotCurveEdge') {
    const panel = document.getElementById('toolbar')
    const curve = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    if (name === 'dotCurveEdge') { curve.setAttribute('stroke-dasharray', '2 2') }
    var x1 = x + 20
    var y1 = y + 10
    var x2 = x1 + 10
    var y2 = y2 + 10
    var cur = 'M' + x + ' ' + y + 'C' + x1 + ' ' + y1 + ',' + x2 + ' ' + y1 + ',' + (x + 40) + ' ' + (y + 40)
    curve.setAttribute('d', cur)
    curve.setAttribute('fill', 'transparent')
    curve.setAttribute('stroke', 'black')
    curve.addEventListener('click', functions)
    panel.appendChild(curve)
  } else if (name === 'select') {
    const panel = document.getElementById('toolbar')
    const square1 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    const square2 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    const square3 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    const square4 = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
    square1.setAttribute('x', x + 1)
    square1.setAttribute('y', y + 1)
    square1.setAttribute('width', 5)
    square1.setAttribute('height', 5)
    square1.addEventListener('click', functions)
    panel.appendChild(square1)
    square2.setAttribute('x', x + 34)
    square2.setAttribute('y', y + 1)
    square2.setAttribute('width', 5)
    square2.setAttribute('height', 5)
    square2.addEventListener('click', functions)
    panel.appendChild(square2)
    square3.setAttribute('x', x + 34)
    square3.setAttribute('y', y + 34)
    square3.setAttribute('width', 5)
    square3.setAttribute('height', 5)
    square3.addEventListener('click', functions)
    panel.appendChild(square3)
    square4.setAttribute('x', x + 1)
    square4.setAttribute('y', y + 34)
    square4.setAttribute('width', 5)
    square4.setAttribute('height', 5)
    square4.addEventListener('click', functions)
    panel.appendChild(square4)
  } else if (name === 'Delete') {
    const panel = document.getElementById('toolbar')
    const remove = document.createElementNS('http://www.w3.org/2000/svg', 'text')
    remove.setAttribute('x', x - 5)
    remove.setAttribute('y', y + 27)
    remove.setAttribute('style', 'cursor: pointer;')
    const txt = document.createTextNode('Delete')
    remove.appendChild(txt)
    remove.addEventListener('click', functions)
    panel.appendChild(remove)
  } else if (name === 'object') {
    const panel = document.getElementById('toolbar')
    const addObject = document.createElementNS('http://www.w3.org/2000/svg', 'text')
    addObject.setAttribute('x', x - 5)
    addObject.setAttribute('y', y + 27)
    addObject.setAttribute('style', 'cursor: pointer;')
    const txt = document.createTextNode('Object')
    addObject.appendChild(txt)
    addObject.addEventListener('click', functions)
    panel.appendChild(addObject)
  }
}

/**
 * Generate and initialize the tool bar
 */
function createToolbar () {
  createButton(0, 'circleW', function () { addNode('circle', 'white') })
  createButton(50, 'circleB', function () { addNode('circle', 'black') })
  createButton(100, 'diamond', function () { addNode('diamond', 'white') })
  createButton(150, 'object', function () { addNode('object') })
  createButton(200, 'line', function () { chooseFunction('line') })
  createButton(250, 'dotline', function () { chooseFunction('dotline') })
  createButton(300, 'HVEdge', function () { chooseFunction('HVEdge') })
  createButton(350, 'dotHVEdge', function () { chooseFunction('dotHVEdge') })
  createButton(400, 'VHEdge', function () { chooseFunction('VHEdge') })
  createButton(450, 'dotVHEdge', function () { chooseFunction('dotVHEdge') })
  createButton(500, 'CurveEdge', function () { chooseFunction('CurveEdge') })
  createButton(550, 'dotCurveEdge', function () { chooseFunction('dotCurveEdge') })
  createButton(600, 'select', function () { chooseFunction('selected') })
  createButton(650, 'Delete', function () { chooseFunction('Delete') })
}

/**
 * create node in the graph editor
 * @param {node} node type of node
 * @param {color} color color of node
 */
function addNode (node, color) {
  movement = 'Node'
  // console.log(movement)
  if (node === 'circle' && color === 'white') {
    graphFrame.add(createCircleNode(10, 10, 20, 'white'))
  } else if (node === 'circle' && color === 'black') {
    graphFrame.add(createCircleNode(10, 10, 20, 'black'))
  } else if (node === 'diamond' && color === 'white') {
    graphFrame.add(createDiamondNode(10, 10, 20, 'white'))
  } else if (node === 'object') {
    graphFrame.add(createObjectNode(Math.floor(Math.random() * 400), Math.floor(Math.random() * 200)))
  }
  graphFrame.draw()
  movement = 'done'
}
